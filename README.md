

[Install link for desktop Chrome + Tampermonkey](https://gitlab.com/ruslan.levitskiy/NotifyOPR/raw/master/NotifyOPR.user.js)

On Android devices, you need to install [Firefox](https://play.google.com/store/apps/details?id=org.mozilla.firefox) and [Tampermonkey Firefox add-on](https://addons.mozilla.org/ru/firefox/addon/tampermonkey/). After that, paste the userscript URL `https://gitlab.com/ruslan.levitskiy/NotifyOPR/raw/master/NotifyOPR.user.js` into the Firefox address bar and confirm the installation.  